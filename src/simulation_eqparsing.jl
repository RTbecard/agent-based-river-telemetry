#    This file is part of ABRiverTelem.
#
#    ABRiverTelem is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ABRiverTelem is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ABRiverTelem.  If not, see <https://www.gnu.org/licenses/>. 
#
#    Copyright 2021 James Campbell
# =============================================================================
# Functions for converting user provided expressions into julia functions.
# Users must provide expressions to update agent states.  These expressions
# are convert to functions whice are used in by the simulation funtions
# =============================================================================



# ========================= Internal functions ================================


"""
    levels(dataframe, variable)

Return a list of unique values for the specified catagorical variable.
"""
function values(covdct::CovariateDict, var::Symbol)
    vals = Vector()
    for reach in values(covdct)
        if var ∈ keys(reach)
            append!(vals, reach[var])
        end
    end
    return unique(vals)
end

"""
    expr2func(expr, vars)

Convert an expression and a tuple of variables into a function which executes
the expression and takes the variables specified in `vars` as a set of keyword 
arguments.

This is used to convert user-supplied expressions info functions which can be 
used in the simulations.
"""
function expr2func(expr::Expr, vars::Vector{Symbol})

    args = Expr(:tuple, vars...)
    f_expr = Expr(:function, args, expr)
    func = eval(f_expr)
    return func

end

"""
    eqparser(expression)

For a given expression return a tuple holding all symbols and all symbols 
representing catagorical varaibles.
"""
function eqparser(e, out = Vector{Symbol}(), catagories = Vector{Pair{Symbol, Symbol}}())
   
    print(e)

    if typeof(e) == Symbol
        push!(out, e)
    elseif typeof(e) == Expr
        if e.head == :block
            for line in e.args
                eqparser(line, out, catagories)
            end
        elseif e.head == :ref
            append!(out, e.args)
            push!(catagories, e.args[1] => e.args[2])
        else 
            # Recurse into expression arguments
            for arg in e.args 
                eqparser(arg, out, catagories)
            end
        end
    end
    return unique(out), catagories
end

"""
Return a vector of arguments
"""
function eq_cov_arguments(model, covdct::CovariateDict; meta = nothing)

    sym, cs = eqparser(model.f)

    # ------ Check that catagorical betas have sufficient levels
    for c in cs
        prms = keys(model.β[c.first])
        if !⊆(prms, values(covdct, c.second)) && !⊆(model, values(meta, c.second))
            @error """
            $name β keys for $c are missing.
            β keys: $(keys(model.β[c.first]))
            CovariateDict values: $(levels(covdct))
            Metadata values: $(levels(covdct))
            """
            error("Model parameters do not fit to dataset")
        end
    end
    
    # ------ Get symbols from covariate names
    cov_syms = covariates(covdct)
    if isnothing(meta)
        meta = DataFrame()
    end
    if size(meta)[1] > 0
        # Get medatata covariates if included
        append!(cov_syms, Symbol.(names(meta)))
    end
    # Append state varaibles
    append!(cov_syms, [:running, :velocity])

    args = vcat(filter(x -> x ∈ cov_syms, sym))
    
    return args
end
