#    This file is part of ABRiverTelem.
#
#    ABRiverTelem is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ABRiverTelem is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ABRiverTelem.  If not, see <https://www.gnu.org/licenses/>. 
#
#    Copyright 2021 James Campbell
# =============================================================================
# Misclanious internal functions for loading data.
# =============================================================================

"""
    loadxml(xml)

Load and validate an XML file.
"""
function misc_loadxml(xml)

    # ------ Expand paths
    file = misc_expandpath(xml)
    path = dirname(file)
    !isfile(file) && error("xml: Invalid file path.")
    doc = readxml(file)
    
    # ------ Load ABRiverTelem DTD
    file_covdtd = joinpath(@__DIR__, "../", "extra","covariatemeta.dtd")
    covdtd = readdtd(file_covdtd)
    # Validate XML
    val = validate(doc, covdtd)

    # Show XML errors
    if length(val) > 0
        show(stdout, MIME"text/plain"(),val)
        print("\n")
        error("Invalid covariate XML file.  Check formatting.")
    end

    return doc
end

"""
    readtabular(file)

Supports either CSV or RDS (R serialized single object).  For CSV files, dates
are expected to be in ISO format, where timezones are ignored.  Returns a
DataFrame object.  RDS files must contain a single data.frame object.
"""
function misc_readtabular(file::AbstractString; csvargs...)

    # ------ Expand file path
    file = misc_expandpath(file)

    if !isfile(file)
        @error """
        Check file path is correct:
        $(file)
        """
        error("File not found: $(basename(file))")
    end

    try
        # ------ Try loading as csv
        fmt = "yyyy-mm-ddTHH:MM:SS"
        df = CSV.File(file, dateformat=fmt; csvargs...) |> DataFrame
        return df
    catch
        try
            # ------ Try loading as RDS
            df = RData.load(file, convert=true)
            # Extract first item in dict (RDS)
            if typeof(df) ≢ DataFrame
                @error """
                RDS files must contain a single dataframe object.  The current file returned type: $(typeof(df))
                """
                error("Invalid RDS file: $(basename(file))")
            end
            return df
        catch
            @error """
            CSV files (with ISO formatted date strings) or RDS (seriealized R objects) are accepted formats.
            """
            error("Cannot read tabular data in : $(basename(file))")
        end
    end
end

"""
    misc_expandpath(paths::Vararg{AbstractString})

Join paths and expand to absolute directory
"""
function misc_expandpath(paths::Vararg{AbstractString})

    return joinpath(paths...) |> expanduser |> abspath

end


"""
Gereral functions used to loading data into the model.
"""

"""
    misc_adjustTimeZone(time::DateTime, tz_covarr::AbstractString,
        tz_target::AbstractString)

Wrapper for converting a DateTime obejct to a different timezone.

This function is typically called after "readreachcovs" to match the time stamps
of the covariate datasets to those of the array detections data.
"""
function misc_adjusttimezone(time::DateTime, tz_source::AbstractString,
        tz_target::AbstractString)

    zdt = ZonedDateTime(time, TimeZone(tz_source))
    zdt_target = astimezone(zdt, TimeZone(tz_target))
    return DateTime(zdt_target)
end

"""
    misc_makeindentifier!(dataframe, columnname)

Convert a column of a dataframe into a Symbol type for use as an identifier
column.  Numeric types will be conveted to integers if possible to remove
trailing ".0"s.  Trailing zeros may happen when R automatically saves an integer
as a float in an RDS file.
"""
function extract_makeidentifier!(dataframe, col)

    vec = dataframe[!, col]

    # Do nothing if already symbols
    typeof(vec) == Symbol && return

    # Convert to ingeters if numeric
    if isa(vec, Vector{<:Number}) && all(isinteger.(vec))
        vec = convert(Vector{Int64}, vec)
    end

    # Convert to symbols and update column
    dataframe[!, col] = Symbol.(vec)

end
