#    This file is part of ABRiverTelem.
#
#    ABRiverTelem is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ABRiverTelem is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ABRiverTelem.  If not, see <https://www.gnu.org/licenses/>.
#
#    Copyright 2021 James Campbell
# =============================================================================
# Functions for extracting data from data objects
# =============================================================================

# ============================ Covariate Arrays ===============================
# ------ Helper types for the get function (below)
TimeIdxTuple = Union{Tuple{Union{DateTime, Nothing},
                           Union{DateTime, Nothing}},
                     DateTime}
DistanceIdxTuple = Union{Tuple{Union{Float64, Nothing},
                               Union{Float64, Nothing}},
                         Float64}

"""
    get(array::CovariateArray, time, distance)

Extract a covariate value, or array of covairate values, from a CovariateArray.
time::DateTime and distance::Float can be either a single value, or a tuple of two values indicating the range to extract from.  passing `nothing` will get all
avaiable values for either time or distance.
"""
function get(array::CovariateArray, time::TimeIdxTuple, distance::DistanceIdxTuple)

    # ------ Convert single values to tuples
    typeof(time) == DateTime && (time = (time, nothing))
    typeof(distance) == DateTime && (distance = (distance, nothing))

    # ------ Remove unnecessary indexes
    size(array.data)[2] == 1 && (distance = (nothing, nothing))
    size(array.data)[1] == 1 && (time = (nothing, nothing))

    # ------ Get time index
    if time[1] == nothing
        time_idx = Colon()
    elseif time[2] == nothing
        t = convert(Float64, Dates.value(time[1] - array.timestart))/1000
        time_idx = convert(Int64, floor(t/array.timedelta) + 1)
    else
        t1 = convert(Float64, Dates.value(time[1] - array.timestart))/1000
        t1 = floor(t1/array.timedelta) + 1
        t2 = convert(Float64, Dates.value(time[2] - array.timestart))/1000
        t2 = floor(t2/array.timedelta) + 1
        time_idx = convert(Int64, t1):convert(Int64, t2)
        if t2 > size(array.data)[1]
            @error """
            Sample time index $t2 out of CovariateArray bounds ($(size(array.data)))
            """
            throw(BoundsError(array.data))
        end
    end

    # ------ Get distance index
    if distance[1] == nothing
        distance_idx = Colon()
    elseif time[2] == nothing
        distance_idx = convert(Int64, floor(distance[1]/array.distancedelta) + 1)
    else
        d1 = convert(Int64, floor(distance[1]/array.distancedelta) + 1)
        d2 = convert(Int64, floor(distance[2]/array.distancedelta) + 1)
        distance_idx = d1:d2
        if d2 > size(array.data)[2]
            @error """
            Sample distance index $d2 out of CovariateArray bounds ($(size(array.data)))
            """
            throw(BoundsError(array.data))
        end
    end

    return array.data[time_idx, distance_idx]
end

function get(array::CovariateArray, time::Nothing, distance::DistanceIdxTuple)
    get(array, (nothing, nothing), distance)
end

function get(array::CovariateArray, time::TimeIdxTuple, distance::Nothing)
    get(array, time, (nothing, nothing))
end


# ============================== Covariate Dict ===============================
"""
    covariates(covs::CovariateDict)

Prints a list of covariate names contained in the object.  This is useful for
letting users know what covariate terms are available for writing the state
update expression.
"""
function covariates(covd::CovariateDict)
    covs = Vector{Symbol}()
    for reachvals in values(covd)
        for cov in keys(reachvals.reachcovs)
            if  cov ∉ covs
                push!(covs, cov)
            end
        end
    end
    return covs
end

# ================================= Meta data =================================
"""
    covatiates(meta::DataFrame)

Return a list of covariate names contained in the agent matadata table.
This is useful for checking which covariate names are avaialbe to users
for writing their state update expression.
"""
function covariates(meta::DataFrame)
    return Symbol.(names(meta))
end

"""
    levels(dataframe, variable)

Return a list of unique values for the specified catagorical variable.
"""
function levels(df::DataFrame, var::Symbol)
    return Symbol.(unique(df[!, var]))
end
