"""
Convert a NetCDF4 file into a time x distance covariate matrix
for use in ABM.
"""

# Standard python libraries
import itertools as it
from datetime import datetime as dt
from datetime import timedelta as td
import logging as log
# Read geometry files
import geopandas as gpd
import netCDF4
# netCDF4 requires libeccodes-dev on ubuntu
# Work with raster data
import numpy as np
import xarray
# GIS
import shapely
import pyproj
import fiona

def py_netcdfSampler(stream_id, distance_step,
        stream_file, stream_id_attribute,
        nc_file, nc_var_x, nc_var_y, nc_var_time, nc_var_sample, nc_bin_mark,
        nc_crs_proj4,
        tz_detections, tz_nc):
    """
  Sample covariate stream data from netCDF4 file.

  netcdfSampler(stream_id, distance_step,
    stream_file, stream_id_attribute,
    nc_file, nc_var_x, nc_var_y, nc_var_time, nc_var_sample, nc_bin_mark,
    tz_detections, tz_nc)

  For a specified stream (LINESTRING) in the provided geopackage file, sample
  points will be marked at equally spaced intervals.  Covariates will then be
  extracted for these sample points from the provided NetCDF4 file at the time
  resolution of that file.

  The result is a 2D numpy array (shape=[time, dowstream distance]) which will
  be used in the ABRiverTelemetry.jl simulations.  A timestamp (ISO formatted
  string) which marks the start of the time dimension and a float which
  represents the timestep (seconds) are also returned.

  In the NetCDF4 file, all variables are expected to be found in the root group.
  Time data is expected to be encoded in the UNIX standard (seconds after
  1970-1-1).  For the streams geopackage file, each stream is expected to be a
  LINESTRING object with an associated string attribute which can be used as a
  unique identifier.  Valid projections for this file must have 'metre' units.

  This code was written in python as julia currently has very limited support
  for GIS operations.

  Parameters
  ----------
  Sample
    - stream_id: The id string of the stream to sample covariates for.
    - distance_step: The interval in meters between downstream sample points.

  Stream file
    - stream_file: String holding the path to the geopackage file holding
        streams as LINESTRING geometries.
    - stream_id_attribute: String holding the geopackage attribute/column name
        which holds the unique identifier string for each stream.

  NetCDF4 file
    - nc_file: Path to NetCDF4 file.
    - nc_var

  Timezone
    - tz_nc: String holding timezone of NetCDF file (ex. '+08:00').
    - tz_detections: String holding timezone of detections data.  This will be
        the output format of the time data.
  """

  # ------ Load datasets
    log.info("Reading Streams geopackage.")
    streams = gpd.read_file(stream_file)
    log.info("Reading NetCDF4 file.")
    nc = netCDF4.Dataset(nc_file)
    ncx = xarray.open_dataset(nc_file)

    tmp = ncx.variables[nc_var_time].data
    log.info("NetCDF date range: %s to %s",
            np.datetime_as_string(tmp[0]), np.datetime_as_string(tmp[-1]))
    tmp = ncx.variables[nc_var_sample]
    log.info("NetCDF data range: %s to %s", tmp.min().data, tmp.max().data)
    # ------ Get CRS of streams data
    src = fiona.open(stream_file)
    src.meta.keys()
    stream_crs = src.meta["crs"]['init']

  # ====================== Prepare input NetCFD file ============================
  # ------ Check that specified variables are in nc file
    vrs = [nc_var_x, nc_var_y, nc_var_time, nc_var_sample]
    ck = tuple(map(lambda x: x in nc.variables.keys(), vrs))
    if not all(ck):
        val_missing = tuple(idx for (idx, val) in enumerate(ck) if not val )
        raise ValueError("Variable(s) " +
            ", ".join(vrs[x] for x in val_missing) +
            " not found in netcdf file.")

    # ------ Get time axis
    # Calculate time steps
    times = nc.variables[nc_var_time][:].data
    tmp = np.unique(np.diff(times))
    if tmp.size > 1:
        raise ValueError("NetCDF4 file does not have uniform timesteps.")
    time_step = float(tmp[0])

    # Calculate start time
    time = times[0]
    # Convert to datetimes
    time = dt.utcfromtimestamp(time)
    # Add offset (alighn timesteps with centers of steps)
    time_offset = td(seconds = time_step/2)
    # Set timestamps to beginning of data bins
    if nc_bin_mark == "end":
      log.info("Subtracting time offset of %s", time_offset*2)
      time_start_nc = time - time_offset*2
    elif nc_bin_mark == "mid":
      log.info("Adding time offset of %s", time_offset)
      time_start_nc = time - time_offset
    elif nc_bin_mark != "start":
      error("Invalid value for `nc_bin_mark`")

    # ------ Convert to export timezone
    try:
        nc_tz = dt.strptime(tz_nc, "%z").tzinfo
    except Exception as e:
        print(e)
        raise ValueError(
          "Invalid timezone format provided for " +str(nc_file)) from e

    try:
        detections_tz = dt.strptime(tz_detections, "%z").tzinfo
    except Exception as e:
        print(e)
        raise ValueError(
          "Invalid timezone format provided for detections file") from e

    # Make netcdf start time tz aware
    time_start_nc = time_start_nc.replace(tzinfo = nc_tz)
    # Change timezone
    time_start_det = time_start_nc.astimezone(detections_tz)

    # ============== Get interpolation points from stream ========================
    # ------Verify that units are in meters for x and y axis
    if not all(map(lambda x: x.unit_name == 'metre', streams.crs.axis_info)):
        raise ValueError(stream_file + \
                " must be a 2D (XY) projected coordinate system" + \
                " with metre units (such as UTM).")
        # ------ Check valid id column name
    if not stream_id_attribute in streams.columns:
        raise ValueError("Invalid stream_id_attribute.  Column '" + \
                stream_id_attribute +"' not found in '" + stream_file + "'.")

      # ------ Get points along stream to extract NetCDF4 data for
    # Get row index of selected stream
    row = next((i for (i, v) in enumerate(streams[stream_id_attribute])
        if v == stream_id), None)
    # Throw error if stream not found
    if row is None:
        txt = "Stream id '" + stream_id +"' not found in " + stream_file + "."
        raise ValueError(txt)
    # Isolate stream
    stream = streams.geometry[row]

    # ------ Check that stream is a LINESTRING
    if not isinstance(stream, shapely.geometry.linestring.LineString):
        raise ValueError(
          "Stream " + stream_id + " is not a LINESTRING feature." + \
                "  Detected stream type: " + type(stream).__name__)

      # ------ Mark sample points along stream
    seq = np.arange(0, stream.length, distance_step)
    sample_pts = [stream.interpolate(d) for d in seq]

    # ------ Convert sample coords to CRS of NetCDF4 file
    # Get crs from proj4 string
    crs = pyproj.CRS.from_proj4(nc_crs_proj4)
    crs_stream = pyproj.Proj(stream_crs)
    # Calculate projection transformation
    project = pyproj.Transformer.from_proj(
          crs_stream, # source coordinate system
          crs) # destination coordinate system

    # ------ Apply CRS transformation (reproject to netcdf CRS)
    sample_pts_trns = list(map(
        lambda x: shapely.ops.transform(project.transform, x), sample_pts))

    # ======================= Build output array =================================
    rows = nc.dimensions[nc_var_time].size
    columns = len(sample_pts_trns)
    out = np.zeros((rows, columns), dtype="float")

    # ------ Build indexing array
    # Make fast access objects to speed up loop times
    lon_arr = np.array(list(map(lambda x: x.x, sample_pts)))
    lat_arr = np.array(list(map(lambda x: x.y, sample_pts)))
    time_mem = ncx.coords[nc_var_time].copy(deep=True).data
    # init empy arrays for filling
    t_samp = np.zeros([rows*columns], dtype=ncx.coords[nc_var_time].dtype)
    x_samp = np.zeros([rows*columns], dtype='float')
    y_samp = np.zeros([rows*columns], dtype='float')
    # Fill inexing arrays
    for (r, c) in it.product(range(0, rows), range(0, columns)):
        i = r*columns + c
        #i = c*rows + r
        x_samp[i] = lon_arr[c]
        y_samp[i] = lat_arr[c]
        t_samp[i] = time_mem[r]

    # ------Use point indexing to extract variable from netcdf object
    # make point index dictionary (dict of xarrays with named dimension)
    idx = {nc_var_time : xarray.DataArray(t_samp, dims = "downstream"),
          nc_var_x : xarray.DataArray(x_samp, dims = "downstream"),
          nc_var_y : xarray.DataArray(y_samp, dims = "downstream")}
    # Extract nearest locations from netcdf file
    sample = ncx.sel(idx, method='nearest')

    out.flat[:] = sample['ssrd'].data

    return (out, time_start_det.strftime("%Y-%m-%dT%H:%M:%S"),
            float(time_step))


def py_getStreamLength(stream_id, stream_file, stream_id_attribute):

    log.info("Reading Streams geopackage.")
    streams = gpd.read_file(stream_file)

    # Find stream in geopackage
    row = next((i for (i, v) in enumerate(streams[stream_id_attribute])
        if v == stream_id), None)

    # Throw error if stream not found
    if row is None:
        txt = "Stream id '" + stream_id +"' not found in " + stream_file + "."
        raise ValueError(txt)
    # Isolate stream
    stream = streams.geometry[row]

    return stream.length
