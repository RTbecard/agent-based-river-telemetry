struct Tracker
    agent::Vector{Symbol}
    duration::Vector{Float64}
    positions::Matrix{Float64}
    running::Matrix{Bool}
    participation::Matrix{Bool}
    reaches::Vector{ReachTuple}
    reach_covariates::Matrix{Union{Nothing, Tuple{Vararg{Union{Float64, Symbol, Nothing}}}}}
    reach_covariates_names::Tuple{Vararg{Symbol}}
    timestart::Tuple{Vararg{DateTime}}
    function Tracker(n_steps, runvec::RunVector, timedelta, covbuff::CovariateBuffer)

        n_agents = length(runvec)
        
        # Init mutable fields
        agent = Vector{Symbol}(undef, n_agents)
        agent .= (x -> x.agent).(runvec)
        duration = Vector{Float64}(undef, n_steps)
        positions = Matrix{Float64}(undef, n_steps, n_agents)
        running = Matrix{Bool}(undef, n_steps, n_agents)
        participation = Matrix{Bool}(undef, n_steps, n_agents)
        reaches = Vector{ReachTuple}(undef, n_agents)
        reaches .= (x -> x.reaches).(runvec)
        reach_covs = Matrix{Union{Nothing, Tuple{Vararg{Union{Float64, Symbol}}}}}(undef, n_steps, n_agents)
        
        # Elapsed run time
        duration[:] .= (1:n_steps)*timedelta
        # Labels for tracked reach covairates
        reach_covariates_names = Tuple(Symbol.(covbuff.k[[covbuff.idx_reachcovs...]]))
        #get stat times
        timestart = Tuple((x -> x.timestart).(runvec))

        new(agent, duration, positions, running, participation,
            reaches, reach_covs, reach_covariates_names, timestart)
    end
end

function Base.show(io::IO, ::MIME"text/plain", tracker::Tracker)
    println("Tracker")
    str = ["Agents: " string(size(tracker.running)[2]);
           "Steps: " string(length(tracker.duration));
           "Tracking Duration:" string(tracker.duration[end]/(60*60), "h")]
    pretty_table(io, str, tf = tf_borderless, noheader = true)
end

function update(tracker::Tracker, step::Int64, rn_i::Int64, rn::Run, 
        covdct::CovariateDict, covbuff)

    # Get position
    function getpos(covdct, reaches, reach_idx, pos)
        if reach_idx > 1
            pos += sum((x -> covdct[x].length).(reaches[1:(reach_idx-1)]))
        elseif reach_idx == - 1
            pos = sum((x -> covdct[x].length).(reaches[1:end]))
        end
        return pos
    end
    pos = getpos(covdct, rn.reaches, rn.state.reachidx, rn.state.reachposition)

    # Get state
    if rn.state.participation
        tracker.positions[step, rn_i] = pos
        tracker.running[step, rn_i] = rn.state.running
        tracker.participation[step, rn_i] = rn.state.participation
        tracker.reach_covariates[step, rn_i] = 
            Tuple(covbuff.v[[covbuff.idx_reachcovs...]])
    else
        # Set remaining values to false
        tracker.running[step, rn_i] = true
        tracker.running[(step+1):end, rn_i] .= false
        tracker.participation[step:end, rn_i] .= false
        tracker.positions[step:end, rn_i] .= pos
    end

end

function plot(track::Tracker, i::Int64, vars...; cols = nothing, 
        covdct = nothing, distancedelta = 100)

    fig = Figure()
    grid = fig[1, 1] = GridLayout()

    # Define plot layout
    vars = length(vars) == 0 ? [ nothing ] : vars
    n = length(vars)
    cols = isnothing(cols) ? floor(sqrt(n)) : cols
    rows = ceil(n / cols)

    # ---------- Common plot features
    i_end = findlast(track.participation[:, i])
    time = [0;track.duration[1:i_end]] ./ (60*60)# convert to hours
    pos = [0; track.positions[1:i_end,i]]
    running = Bool.([true; track.running[1:i_end,i]])
    g = cgrad([:tomato, :seagreen3], 2, categorical = true)
    
    # ---------- loop plots top make
    for (plot_i, vr) in enumerate(vars)

        f_row = Int(ceil(plot_i / cols))
        f_col = Int(plot_i - (f_row-1)*cols)

        # Define primary axis
        aw_wrap = grid[f_row, f_col] = GridLayout()
        
        ax = Axis(aw_wrap[1,1])
        tightlimits!(ax)
    
        if !isnothing(vr)
            # Check if variable present
            idx = findfirst(x -> x == vr, track.reach_covariates_names)
            isnothing(idx) && error("Covariate $vr not in tracking data.")

            # Define second axis
            axr = Axis(aw_wrap[1,1])
            tightlimits!(axr)
            #axr = Axis(grid[f_row, f_col])
            hidexdecorations!(axr)
            axr.ylabel = string(vr)
            
            if isnothing(covdct)
                # ------ Line plot of covariate
                # Extract variable
                cv = (x -> x[idx]).(track.reach_covariates[1:i_end, i])
                cv = [ cv[1]; cv]
                hideydecorations!(axr, label = false, ticks = false, 
                                  ticklabels = false, minorticks = false)
                axr.yaxisposition = :right
                lines!(axr, time, cv, color = :steelblue, colormap = g)
            else
                # ------ Heatmap of covariates
                dists = (distancedelta*0.5):distancedelta:(pos[end] - distancedelta*0.5)
                #dists = 0:distancedelta:pos[end]
                reach_lens = (x -> covdct[x].length).(track.reaches[i])
                reach_lens_cml = cumsum([0, reach_lens...])
                reach_idxs = [findlast(r -> d - r >= 0, reach_lens_cml)
                                for d in dists]
                dists_reach = dists .- reach_lens_cml[reach_idxs]
                
                t_delta = mean(diff(time))*0.5
                time2 = time[2:end] .- t_delta  
                n_d = length(dists_reach)
                n_t = length(time2)
                n =  n_d * n_t
                map_d = Vector{Float64}(undef, n)
                map_t = Vector{Float64}(undef, n)
                map_v = Vector{Union{Float64}}(undef, n)
                for (d_i, d) in enumerate(dists_reach)
                    # get covariate array
                    covarr = covdct[track.reaches[i][reach_idxs[d_i]]][vr]
                    s_offset = Second(track.timestart[i] - covarr.timestart).value
                    for (t_i, t) in enumerate(time2)
                        n_i = ((d_i - 1) * n_t) + t_i
                        map_d[n_i] = dists[d_i]
                        map_t[n_i] = t_i
                        s = (t - t_delta)*60*60 + s_offset 
                        covarr_t_i = Int(ceil(s / covarr.timedelta)) 
                        if  isnothing(covarr.distancedelta)
                            covarr_d_i = 1
                        else
                            covarr_d_i = max(1, 
                                        Int(ceil(d / covarr.distancedelta)))
                        end
                        map_v[n_i] = covarr.data[covarr_t_i, covarr_d_i]
                    end
                end
                
                # the heatmap xaxis without white gaps
                hideydecorations!(axr)
                hm = heatmap!(axr, map_t, map_d, map_v, padding = (0.0, 0.0))
                #hm = heatmap!(ax, map_t*mean(diff(time)) , map_d, map_v)
                
                #Heatmap colorbar
                Colorbar(aw_wrap[1,2], hm, label = string(vr))

            end
        end

        # ------ Line plot
        lines!(ax, time, pos, color = :black,
               linewidth = 4, padding = (0.0, 0.0))
        lines!(ax, time, pos, color = running, colormap = g,
               linewidth = 3, padding = (0.0, 0.0))
        ax.xlabel = "Time (h)"
        ax.ylabel = "Distance (m)"
        if f_row != rows 
            hidexdecorations!(ax, grid = false)
        end
        
        # ------ Legend
        args = (linestyle=nothing, points=Point2f[(0, 0.5), (1, 0.5)])
        if isnothing(covdct)
            legend_names = ["Halting", "Running", "Covariate"]
            legend_elements = [
               LineElement(;color = :tomato, args...)
               LineElement(;color = :seagreen3, args...)
               LineElement(;color = :steelblue, args...) ]
        else
            legend_names = ["Halting", "Running"]
            legend_elements = [
               LineElement(;color = :tomato, args...)
               LineElement(;color = :seagreen3, args...) ]
        end
        Legend(fig[2,1], legend_elements, legend_names,
               orientation = :horizontal, tellwidth = false, tellheight = true)
    end

    fig[0, :] = Label(fig,
                "Agent: $(track.agent[i]), Start Time: $(track.timestart[i])")

    return fig

end

"""
    plot(tracker, vars; cols = nothing)

Plot univariate and bivariate histograms of the running/halting states of the
simulated agents.
"""
function plot(track::Tracker, vars...; cols = nothing, bins = 30, levels = 10)
    
    # Init figure object
    fig = Figure()
    
    # ------ Calculate pairs of covariates for histograms
    covs = Vector{Tuple{Vararg{Symbol}}}([tuple.(vars)...])
    for i in 1:(length(vars) - 1)
        for j in i+1:length(vars)
            push!(covs, (vars[i], vars[j]))
        end
    end
    covs

    # Define plot layout
    n = length(covs)
    cols = isnothing(cols) ? round(sqrt(n)) : cols
    rows = ceil(n / cols)

    #Get all covariate data for an agent
    function getcov(cv, track, agent_i)
        data = []
        for i in agent_i
            cv_i = findfirst(x -> x == cv, track.reach_covariates_names)
            isnothing(cv_i) && error("Covariate :$cv not in tracking data")
            i_end = findlast(x -> x == true, track.participation[1:end, i])
            isnothing(i_end) && continue
            ps = (x -> x[cv_i]).(track.reach_covariates[1:i_end, i])
            append!(data, ps)
        end
        return data
    end
    # calculate bin edges for histograms
    function hist_edgs(cv, track, agents_n)
        data = getcov(cv, track, 1:agents_n)
        mn = minimum(data)
        mx = maximum(data)
        return (0:(1/bins):1)*(mx - mn) .+ mn
    end
    # Calculate noramlized histograms
    function mk_histogram(hist_mean, cv, track, agents_n, hist_edge; data_idx = nothing)
        if isnothing(data_idx)
            return hist_mean
        end
        if length(cv) == 1
            data = getcov(cv[1], track, 1:agents_n) 
            hist = fit(Histogram, data[data_idx], hist_edge[1])
            out = hist.weights/sum(hist.weights)
        else
            data = (getcov(cv[1], track, 1:agents_n)[data_idx],
                    getcov(cv[2], track, 1:agents_n)[data_idx])
            hist = fit(Histogram, data, hist_edge)
            out = hist.weights/sum(hist.weights)
        end
        if isnothing(hist_mean) 
            return out
        else
            hist_mean .+= out 
            return hist_mean
        end
    end

    agents_n = length(track.agent)
    hist_edges = []
    for cv in covs
        if length(cv) == 1
            edge = (hist_edgs(cv[1], track, agents_n),)
        else
            edge = (hist_edgs(cv[1], track, agents_n),
                    hist_edgs(cv[2], track, agents_n))
        end
        push!(hist_edges, edge)
    end

    c = string("rgba(", join(Colors.color_names["seagreen"], ","),",0.5)")
    col_r = parse(RGBA, c)
    c = string("rgba(", join(Colors.color_names["tomato"], ","),",0.5)")
    col_h = parse(RGBA, c)
    
    for (cv_i, cv) in enumerate(covs)
        f_row = Int(ceil(cv_i / cols))
        f_col = Int(cv_i - (f_row-1)*cols)
        hist_mean_r = nothing
        hist_mean_h = nothing
        for agent_i in 1:agents_n
            #println(agent_i)
            # find last sample for agent
            i_end = findlast(track.participation[1:end, agent_i])
            isnothing(i_end) && continue
            idx_run = findall(track.running[1:i_end, agent_i])
            idx_halt = findall(track.running[1:i_end, agent_i] .== 0)
            hist_mean_r = mk_histogram(hist_mean_r, cv, track, agents_n, 
                                       hist_edges[cv_i]; data_idx = idx_run)
            hist_mean_h = mk_histogram(hist_mean_h, cv, track, agents_n, 
                                       hist_edges[cv_i]; data_idx = idx_halt)
        end
        # Normalize
        hist_mean_r .= hist_mean_r ./ sum(hist_mean_r)
        hist_mean_h .= hist_mean_h ./ sum(hist_mean_h)
        ax1 = Axis(fig[f_row, f_col])
        
        if length(cv) == 1
            # Plot univariate histogram
            ax2 = Axis(fig[f_row, f_col])
            linkaxes!(ax1, ax2)
            x = hist_edges[cv_i][1]
            x = collect(x[2:end]) .- Float64(x.step)*0.5
            band!(ax1, x, 0, hist_mean_r; transparent = true, color = col_r)
            band!(ax2, x, 0, hist_mean_h; color = col_h)
            ax1.xlabel = string(cv[1])
            ax1.ylabel = "Density"
        else
            # Plot multivariate histogram
            x = hist_edges[cv_i][1]
            x = collect(x[2:end]) .- Float64(x.step)*0.5
            y = hist_edges[cv_i][2]
            y = collect(y[2:end]) .- Float64(y.step)*0.5
            contour!(ax1, x, y, hist_mean_r, color = col_r, levels = levels)
            contour!(ax1, x, y, hist_mean_h, color = col_h, levels = levels)
            ax1.xlabel = string(cv[1])
            ax1.ylabel = string(cv[2])
        end
    end

    args = (linestyle=nothing, points=Point2f[(0,1),(1,1),(1,0),(0,0)])
    legend_names = ["running", "halting"]
    legend_elements = [ PolyElement(;color = :seagreen, args...)
                       PolyElement(;color = :tomato, args...) ] 
    Legend(fig[Int(rows) + 1, :], legend_elements, legend_names;
           orientation = :horizontal, tellwidth = false, tellheight = true)
    
    return fig

end

