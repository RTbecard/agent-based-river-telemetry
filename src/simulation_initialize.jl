#    This file is part of ABRiverTelem.
#
#    ABRiverTelem is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ABRiverTelem is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ABRiverTelem.  If not, see <https://www.gnu.org/licenses/>. 
#
#    Copyright 2021 James Campbell


"""
    initruns(runstats)

Make a RunVector from a RunStatVector.  RunStatVectors are returned when 
detections are read from disk (`readdetections`) and hold the result of an 
agents run.  A RunVector is the object holding the state of an agent during
the simulation.
"""
function initruns(runstats::RunStatVector, running = true)

    #runvec = RunVector()
    runvec = Vector()

    for runstat in runstats
        run = Run(runstat.reaches, runstat.timestart, runstat.agent, 
                  running = running)
        push!(runvec, run)
    end

    #return runvec
    return RunVector(runvec)
end

