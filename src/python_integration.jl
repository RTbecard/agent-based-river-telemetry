"""
Check PyCall.jl settings.

Throw warnings is settings may cause issues with python package dependencies.
"""
function pycall_check()
    # ------ Check that Julia's conda environment is being used
    if !PyCall.conda
        @warn """
        PyCall is currently configured for using the system python distribution.

        PyCall is recomended to be built with an internal Conda distribution for use.
        To change python distributions, run the following commands:
         - `ENV["PYTHON"] = ""`
         - `Pkg.build("PyCall")`
         - Then restart your Julia session.
        """
    end
end


"""
Loop and install Python dependancies

    pycall_install_deps()

If any fail to load/install, throw warning and suggestions on how to solve
issue.
"""
function pycall_install_deps()
    # ------ Check that python package dependancies are met
    @info "Checking python dependancies."
    fails = Vector{String}()
    for pypkg in String["geopandas", "netCDF4", "numpy", "xarray", "shapely", "pyproj", "fiona"]
        # init list of failed packages
        try
            pyimport_conda(pypkg, pypkg, "conda-forge")
        catch e
            push!(fails, pypkg)
        end
    end
    if length(fails) > 0
        txt = """
        Failed to install and load python package(s) $(join(fails, ", ")).
        """
        if PyCall.conda
            txt = string(txt, """\n
                         You may try using your system's python distribution and manually installing
                         the probelmatic packages.  To switch to a system distribution, run the following:
                         - `ENV["PYTHON"] = "/path/to/python_binary"`
                         - `Pkg.build("PyCall")`
                         """)
        end

        txt = string(txt, """\n
                     You can retry the installation of these python dependancies anytime with
                     `pycall_install_deps()`
                     """)
        @warn string(txt)

        ret = false
    else
        ret = true
    end

    return ret
end

"""
Load custom python code in Julia.

This is requried for reading covariate data from NetCDF4 sources.
"""
function pycall_load()
    # ------ Load custom Python function for reading NetCDF4 data
    # Load current dir (src) into python path
    @info "Loading Python function for NetCDF4 data."
    pth = @__DIR__
    py"""
    import sys
    sys.path.insert(0, $pth)
    """
    py"sys.path"
    # Load netcdf reading function
    istrm = pyimport("interpolate_streams")
    # Save function as global variable
    global py_netcdfSampler = istrm.py_netcdfSampler
    global py_getStreamLength = istrm.py_getStreamLength
end
