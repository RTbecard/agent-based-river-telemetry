#    This file is part of ABRiverTelem.
#
#    ABRiverTelem is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ABRiverTelem is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ABRiverTelem.  If not, see <https://www.gnu.org/licenses/>. 
#
#    Copyright 2021 James Campbell
"""
    ReachTuple(("1", "2", ...))

A Tuple for holding the identifying strings of reaches within a Run.
"""
ReachTuple = Tuple{Vararg{Symbol}}

mutable struct RunState
    time::DateTime
    participation::Bool
    reachidx::Int64
    reachposition::Float64
    velocity::Float64
    aux::Any
end

"""
    Run(reaches, timestart, fishid)

A Run describes an agent as its traveling through one or more reaches.  A 
single Run is composed of two subsequent array detections.

The state Dict holds the current distance from start `pos`, the current 
absolute time `time`, and `participation` which can be set to false 
to indicate that the animal is no longer active in the simulation.  For 
example, `participation = false` can be used to indicate the mortality of the 
animal, and `pos` and `time` would then indicate the animals position at the 
time of its death.  Participation may also be used to indicate an animal which
ends its migration early.
"""
struct Run
    reaches::ReachTuple
    timestart::DateTime
    agent::Symbol
    state::RunState
    function Run(reaches::ReachTuple, timestart::DateTime, 
            agent::Symbol; running = true)
        state = RunState(timestart, true, true, 1, 0., 0.)
        new(reaches, timestart, agent, state)
    end
end

function Base.show(io::IO, ::MIME"text/plain", run::Run)

    println(io, string("Run Object"))
    d1 = OrderedDict("Agent ID" => run.agent,
                     "Time start" => run.timestart,
                     "Reaches" => join(run.reaches, ", "))
    pretty_table(io, d1, tf = tf_borderless, noheader=true, alignment=[:r, :l])
    
    println(io, string("State:"))
    d2 = OrderedDict("Current time" => run.state.time,
                     "Duration (s)" => run.state.time - run.timestart,
                     "Participating" => run.state.participation,
                     "Reach index" => run.state.reachidx,
                     "Reach position" => run.state.reachposition,
                     "Running" => run.state.running)
    pretty_table(io, d2, tf = tf_borderless, noheader=true, alignment=[:r, :l])

end


"""
    RunVector((run1, run2, ...))

A vector for holding the Run variables (i.e. agents) used in the model 
simulations.

When fitting observed detection data to the model, the function 
`ABRiverTelem.initruns` will generate a variable of this type from your 
dataset.
"""
#RunVector = Vector{Run}
RunVector = Tuple{Vararg{Run}}

function Base.show(io::IO, ::MIME"text/plain", runvec::RunVector)
    
    # Convert to vector
    runvec = Vector([runvec...])
    # Make display column values
    agents = (x -> string(x.agent)).(runvec)
    starttimes = (x -> string(x.timestart)).(runvec)
    participation = (x -> string(x.state.participation)).(runvec)
    runstate = (x -> string(x.state.running)).(runvec)
    duration = (x -> string((x.state.time - x.timestart).value/(1000*60*60),"h")).(runvec)

    println(io, string("RunVector Object"))
    d1 = OrderedDict("Agent ID" => agents,
                     "Time Start" => starttimes,
                     "Participating" => participation,
                     "Run State" => runstate,
                     "Run Duration" => duration)
    pretty_table(io, DataFrame(d1), tf = tf_borderless, nosubheader=true)

end
"""
    RunStat(timestart, duration, agentid, reaches)

RunStat types hold the summary statistic of an agents run.  Detections data
are loaded as RunStat objects (see readdetections).  When simulations complete,
RunStat objects are created for each run and can be used to geenrate summary statistics for the Agent Based Model.

RunStat objects are also used to initialize simulations.  Here, the field 
duration is allowed to be `nothing`, allowing simulations to be created without
prexisting results to compare the output to.
"""
struct RunStat
    timestart::DateTime
    duration::Union{AbstractFloat, Nothing}
    agent::Symbol
    reaches::ReachTuple
    reachidx::Integer
end

function Base.show(io::IO, ::MIME"text/plain", rs::RunStat)
    println(io, string("RunStat Object"))
    println(io, string("Agent id: ", rs.agent))
    println(io, string("Start: ", rs.timestart))
    println(io, string("Duration (s): ", rs.duration))
    println(io, string("Reaches: ", join(rs.reaches, ", ")))
    println(io, string("Reach Idx: ", rs.reachidx))
end

function Base.show(io::IO, rs::RunStat)
    println(io, string("RunStat(", rs.agent, 
                       "; ", rs.timestart, "; ", 
                      join(rs.reaches), ")"))
end


"""
    RunStatVector(run1, run2, ...)

A vector of RunStat obejcts.   USed for generating summary statistics from model
simulations.
"""
RunStatVector = Vector{RunStat}

function Base.show(io::IO, ::MIME"text/plain", rs::RunStatVector)
    function showreaches(r)
        if length(r) == 1
            return string(r[1])
        else
            return string(r[1], " ... ", r[length(r)])
        end
    end

    tbl = OrderedDict("Agent" => (x -> x.agent).(rs),
               "Start Time" => (x -> x.timestart).(rs),
               "Duration (s)" => (x -> x.duration).(rs),
               "Reaches" => (x -> showreaches(x.reaches)).(rs),
               "Reach Idx" => (x -> x.reachidx).(rs)
               )

    println("RunStatVector Object")
    pretty_table(io, DataFrame(tbl), tf = tf_borderless, nosubheader=true)
end

function Base.show(io::IO, runstats::RunStatVector)
    print(string("RunStatVector(", length(runstats), " RunStats)"))
end

function DataFrames.DataFrame(runstats::RunStatVector)

    DataFrame(agent = (x -> x.agent).(runstats),
              start = (x -> x.timestart).(runstats),
              duration = (x -> x.duration).(runstats),
              reaches = (x -> x.reaches).(runstats))

end
