#    This file is part of ABRiverTelem.
#
#    ABRiverTelem is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ABRiverTelem is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ABRiverTelem.  If not, see <https://www.gnu.org/licenses/>.
#
#    Copyright 2021 James Campbell
# =============================================================================

module ABRiverTelem

# Packages
using LinearAlgebra
using Distributions: Normal, MvNormal, pdf
using Dates, TimeZones
using DataFrames, PrettyTables
using EzXML
using PyCall, Conda
using Printf
using OrderedCollections
import RData, CSV
using GLMakie
using Statistics
using StatsBase: Histogram, fit, normalize, ProbabilityWeights, sample
using Colors
import Serialization
import DensityRatioEstimation.densratiofunc
import DensityRatioEstimation.KLIEP
using ProgressMeter
# Overridden functions
import Base: filter!, show, keys, getindex, values, step, run, reset, get
# Required for dealing with RDS file compression
import CodecBzip2
import CodecXz
# Allow debugging linebreaks in precompiled package
using Debugger

# ------ Load source files
# First order types must be loaded before second order (nested types)
files = [
    # Types
    "types_covariates.jl"
    "types_runs.jl"
    "types_modelparam.jl"
    # "types_simulation.jl"
    # "types_tracking.jl"
    # Methods
    "data_detections_load.jl"
    "data_covariates_load.jl"
    "data_covariates_extract.jl"
    "data_misc.jl"
    # "simulation_eqparsing.jl"
    # "simulation_initialize.jl"
    # "simulation_run.jl"
    # "simulation_results.jl"
    # Python
    "python_integration.jl"
    ]

for file in files
    include(joinpath(@__DIR__, file))
end

export
    # ------ Types
    # Data containers
    Reach, ReachCovariates,
    CovariateArray, CovariateDict,
    RunStat, RunStatVector,
    Run, RunState, RunVector,
    # Model
    StateModel,
    # Simualtion objects
    Simulation, Tracker,
    # ------ Methods
    # Loading data
    load_covariates_reaches,
    load_covariates_agents,
    load_detections,
    # Extract data from objects
    get, covariates, levels,
    # Simulating
    initruns, run,
    reset, setβ


# ------ Setup pycall function (for reading spatial reach data)
function __init__()
    # Run python dependancy checker/installer
    pycall_check()

    pass = pycall_install_deps()

    if pass
        pycall_load()
    else
        @warn """
        Python functions not loaded due to dependancy errors.

        Use `pycall_install_deps()` to try reinstalling the dependancies.  Once
        resolved then you may then use `pycall_load()` to load the custom python
        functions.

        The python functionality is required for reading spatial covariate data from
        NetCDF4 files.
        """
    end
end

end
