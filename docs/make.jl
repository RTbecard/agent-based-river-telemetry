using Documenter, ABRiverTelem

makedocs(sitename="ABRiverTelem.jl",
         pages = [
                  "Overview" => "index.md",
                  "Loading Data" => "data.md",
                  "Running Simulations" => "simulation.md",
                  "Parameter Estimation" => "abc.md"],
        authors = "James Campbell",
        repo = "https://gitlab.com/RTbecard/agent-based-river-telemetry",
        format = Documenter.HTML(prettyurls = false))
