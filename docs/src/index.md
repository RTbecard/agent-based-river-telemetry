# ABRiverTelem.jl

**A**gent **B**ased **River Telem**etry provides a framework for modelling
migrating river fishes.
Specifically, ABRiverTelem deals with whole river migrations monitored by
telemetry detection stations.
Here's a quick summary of the modelling structure:

- Rivers are treated as 1-dimensional spaces.  Agents can only move upstream or downstream.
- **reach**es are the foundational blocks of spatial data which are *spans of river between two subsequent telemetry detection stations*.
- Each reach can have time-varying covariate data consisting of:
  - Spatially dependant rasters.
  - Spatially independent tabular data.
- Agents have states which are a set of attributes that can change during the course of a simulation.  These track the agents position in space and time and may also include user-defined attributes, such as physiology measures.
- Users provide a *state update expression*.  This is a chunk of Julia code which alters the state of the agent at each time step in response to the environmental covariates and the previous state of the agent.
- Agent behaviour is simulated across discreet time-steps, but their movement
    has continuous step lengths.

The framework is deliberately broad to facilitate a variety of migration models.
The most significant restriction is the consideration of rivers as 1 dimensional
spaces.

The utility of ABRiverTelem lies in its 1) ability to automate the formatting of
covariate data from multiple sources, and 2) its machinery for applying Approximate Bayesian Computation to estimate parameter values from telemetry detection data.


