# ------ Participation
model_prt = PrtcpModel(:(b0*1), Dict(:b0 => Inf))

# ------ Run state probability
run_f = quote
    #logit = b0 + b1*discharge + b2*ssrd + z[agent]
    if running == 1.0
        logit = b0_r + b1*ssrd + b2*dssrd + b3*discharge
    else
        logit = b0_h + b1*ssrd + b2*dssrd + b3*discharge
    end
end
betas = Dict(:b0_r => 0., :b0_h => 0., :b1 => 0., :b2 => 0., :b3 =>0.)
model_run = RunModel(run_f, betas)

# ------ Velocity
vel_f = quote
    if running == 1.0
        return max(c[reach]*discharge, 0.1)
    else
        return 0
    end
end
c = Dict(x => 0.01 for x in reaches)
model_vel = VelocModel(vel_f, Dict(:c => c))
