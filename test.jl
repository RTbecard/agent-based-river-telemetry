using Revise
using ABRiverTelem
using BenchmarkTools

expr = quote
    #logit = b0 + b1*discharge + b2*ssrd + z[agent]
    if running == 1.0
        state.running = ilogit(b0_r + b1*ssrd + b2*dssrd + b3*discharge)
    else
        state.running = ilogit(b0_h + b1*ssrd + b2*dssrd + b3*discharge)
    end
end


symbols = ABRiverTelem.eqparser(expr)

v = Vector{Float64}(undef, 3)
v[1:3] .= (1,2,3)
t = (1.0,2.0,3.0)
sa = MVector{3, Float64}(1,2,3)
sa[1:3] .= (1.,2.,3.)

test(x, y , z) = x + y + z 

@btime v[1:3] .= (1,2,3); test(v...)
@btime t = (1.0,2.0,3.0); test(t...)
@btime sa[1:3] .= (1.,2.,3.); test(sa...)

@btime test(v...)
@btime test(t...)
@btime test(sa...)
