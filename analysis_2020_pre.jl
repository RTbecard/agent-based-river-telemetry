# Load and clean metadata for 2020 detections
# Any runs which have missing or outof bounds data will be removed from the
# set.
# The resulting data will be serialized and compressed for local use.
using ABRiverTelem
using Dates
using GZip
using Serialization

# ============ Set metadata file
xml_file = "~/Repos/ribes-esr02_datacuration/data/CCV-JSATS/covariates_2020_final.xml"

# ============ Load covariate data
covs_dict = load_covariates_reaches(xml_file, 100.0)

covs_dict[Symbol("113875979_2020_517")]

# Load detections data
runstats = load_detections(xml_file)
# Load metadata
meta = load_covariates_agents(xml_file, missingstring = "NA")

# ========== Create delta ssrd data
for rch in keys(covs_dict)
    reachcovs = covs_dict[rch].reachcovs
    local ssrd = reachcovs[:ssrd]
    local offset = Second(ssrd.timedelta/2)
    local dssrd = CovariateArray(ssrd.timestart + offset,
                                 ssrd.timedelta,
                                 ssrd.distancedelta,
                                 diff(ssrd.data, dims = 1),
                                 "analysis_2020_pre.jl")
    reachcovs[:dssrd] = dssrd
end

# ============ Find agents with missing covariate data
# mark runs with missing covariate data
cvs = [:discharge :ssrd :dssrd]
skip_idx = []

# loop run stats
for (i, rs) in enumerate(runstats)
    # Loop reaches
    for rch in rs.reaches
        rch_covs = covs_dict[rch].reachcovs
        # Loop covariates to check
        for cv in cvs
            # Check of covariate present in dataset
            if cv ∉ keys(rch_covs)
                # if not, mark index for removal
                println("$i: Covariate data missing.")
                push!(skip_idx, i)
                break
            else
                covarr = rch_covs[cv]
                time_ca = (covarr.timestart,
                           covarr.timestart +
                           Second(covarr.timedelta * size(covarr.data)[1]))
                # extract up to 5 days of data
                time = rs.timestart, rs.timestart + Day(5)
                # check that data is within covariate array
                if time[1] < time_ca[1] || time[2] > time_ca[2]
                    # mark index for removal
                    println("$i: Covariate data outside time.")
                    push!(skip_idx, i)
                    break
                end
                sample = get(rch_covs[cv], time, nothing)
                # Check that there is no missing data in sample
                chk = any((x -> ismissing(x) || isnothing(x)).(sample))
                if chk
                    println("$i: Covariate data missing some values.")
                    # if missing data, mark run stat for removal
                    push!(skip_idx, i)
                    break
                end
            end
        end
        if i ∈ skip_idx
            break
        end
    end
end

# remove indixies to skip
deleteat!(runstats, skip_idx)

# ============ Get velocity constants
include("get_velocity_constant.jl")
vel = getvelconstant(covs_dict, runstats)
print(vel)

# ============ Filter out reaches with less than 100 runs
# linear vector of all ran reaches
rches = vcat((x -> collect(x.reaches)).(runstats)...)
# Filter out >100 run reaches
rches_100 = [k for k in unique(rches) if count(x -> x == k, rches) >= 100]
filter!(x -> x.reaches ⊆ rches_100, runstats)
filter!(x -> x.first ∈ rches_100, covs_dict)

# ============ Add metadata rows for missing runs
agents_runs = (x -> x.agent).(runstats)
agents_meta = unique(meta[!, :agent])
agents_missing = unique(filter(x -> x ∉ agents_meta, agents_runs))
allowmissing!(meta, :)
for agent in agents_missing
    local row = Dict{String, Any}((x -> x => missing).(names(meta))...)
    row["agent"] = agent
    push!(meta, row)
end
# ============ serialize and compress data so it can be quickly loaded later
GZip.open("data_2020.gz", "w") do gf
    serialize(gf, (covs_dict, runstats, meta))
end
