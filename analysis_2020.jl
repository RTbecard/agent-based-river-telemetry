using Revise
using ABRiverTelem
using Debugger
using DataFrames
using Serialization
import GZip
using Dates
using Distributions
using BenchmarkTools
using ProfileView
# Enable debugging messages
#ENV["JULIA_DEBUG"] = ABRiverTelem
# python debugging messages
#using PyCall
#py"""
#import logging as log
#logger = log.getLogger()
#logger.setLevel(log.INFO)
#"""
# ============ Load data from serialized object
# include("analysis_2020_pre.jl")
GZip.open("data_2020.gz", "r") do gf
    global (covs_dict, runstats, meta) = deserialize(gf)
end
# ============ Init runvectors (agents)
runvec = initruns(runstats)
# ============ Make model objects
# run state probability
run_f = quote
    #logit = b0 + b1*discharge + b2*ssrd + z[agent]
    if running == 1.0
        logit = b0_r + b1*ssrd
    else
        logit = b0_h + b1*ssrd
    end
end
vel_f = quote
    if running == 1.0
        return max(c*discharge, 0.05)
    else
        return 0
    end
end
betas = Dict(:b0_r => 0., :b0_h => 0., :b1 => 0.)
prtprm = PrtcpModel(:(b0*1), Dict(:b0 => Inf))
runprm = RunModel(run_f, betas)
velprm = VelocModel(vel_f, Dict(:c => 0.0001))
time_step = 60*10.0
max_iter = Int64(floor(60*60*24*5/time_step))
sim = Simulation(runvec, covs_dict,
                 runprm, prtprm, velprm,
                 time_step, meta = meta)
reset(sim)
track = run(sim, 720, track = true)

plot(track, 1, :ssrd, :discharge, covdct = covs_dict)
plot(track, 1, :ssrd, :discharge, cols = 1)
plot(track, 1, :ssrd, cols = 1)

plot(track, :ssrd)

plot(track, :ssrd, :discharge, bins = 10)

plot(track, :ssrd)


fig

test = function(s)
    reset(s)
    run(s, 720)
end
@btime test($sim)

@profview test(sim)

