using Revise
using ABRiverTelem
using DataFrames
using Serialization
import GZip
import StatsBase.mean
import StatsBase.std
import Distributions.Normal
import Distributions.Uniform
import LinearAlgebra.norm
# =============================== Load dataset ================================
# include("analysis_2020_pre.jl")
GZip.open("data_2020.gz", "r") do gf
    global (covs_dict, runstats, meta) = deserialize(gf)
end
# ------ Init runvectors (agents)
include("analysis_2020_runvec.jl")
runvec
# =========================== Initialize simulation ===========================
# ------ Init models
include("analysis_2020_model.jl")
model_prt
model_run
model_vel
# ------ Init run vector
include("analysis_2020_runvec.jl")
runvec
reaches
# Maximum iterations set 150% maximum run time on any reach
time_step = 60*10.0
max_iter = Int(floor(maximum((x -> x.duration).(runstats_filt))*1.5/(time_step)))
sim = Simulation(runvec, covs_dict, model_prt, model_run, model_vel, time_step,
                 meta = meta)
# ====================== Approximate Bayesian Computation =====================
# --- Initialize model object for running ABC on
# ------ Get value for inital threshold
ϵ = abc_dist_init(model, 1000, priors, 0.15)
result = abc_sm(model, priors, 1000, ϵ; T_thresh = 15, ar_thresh = 0.05)

result2 = abc_sm(model, priors, 1000, result.ϵ̄[end]; T_thresh = 15, ar_thresh = 0.05, append = result)

plot(result)
plot(result, particle = 1)

# Get best model from last analysis
result = deserialize("ABCSM_20210916T204951.jls")
plot(result)


i = argmin(result.d̄)
result.d̄[i]
β = NamedTuple{keys(result.priors)}(result.θ_vec[end][:, i])

reset(model.sim)
f_setβ(model.sim, β)
track = run(model.sim, model.maxiter, track = true)
f_dist(model.sim, x_obs)
model.sim.runvec
plot(track, 60, :ssrd, :discharge)
plot(track, :ssrd, :discharge)

x_sim = [(x -> (x.state.time - x.timestart).value/1000).(model.sim.runvec)...]
hcat(x_obs, x_sim)
sim.runvec[1].state.time
sim.runvec[1].timestart

fieldnames(Tracker)
[track.positions[:,30] track.running[:,30]]
