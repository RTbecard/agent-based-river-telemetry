# Agent Based River Telemetry

[![](https://img.shields.io/badge/docs-stable-blue.svg)](https://rtbecard.gitlab.io/ABRiverTelem.jl)

A framework for applying agent based models (ABMs) to river telemetry datasets.  Tailored for the analysis of diadromous fish migrations, but designed to be extensible for use in other river telemetry applications.
